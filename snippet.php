<?php

/**
 * Add new register fields for WooCommerce registration.
 *
 * @return string Register fields HTML.
 */
function wooc_extra_register_fields() {
	
	$countries = array( '' => __( 'Select a country&hellip;', 'woocommerce' ) ) + WC()->countries->get_allowed_countries();

?>

	<fieldset class="form-table">
		<legend><h3>Member Information</h3></legend>
		<p>Please provide the main contact for this account. Participants can be added once the account is created.</p>
		<p class="form-row form-row-first">
			<label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
		</p>

		<p class="form-row form-row-last">
			<label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
		</p>

		<div class="clear"></div>

		<p class="form-row form-row-wide">
			<label for="reg_billing_phone"><?php _e( 'Phone', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
		</p>

		<div class="clear"></div>

		<h4>Address</h4><p>Please provide the main contact address for this account.</p>
		<p class="form-row form-row-wide">
			<label for="reg_billing_address_1"><?php _e( 'Address', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_address_1" id="reg_billing_address_1" placeholder="Street/P.O Box address" value="<?php if ( ! empty( $_POST['billing_address_1'] ) ) esc_attr_e( $_POST['billing_address_1'] ); ?>" />
		</p>
	  
		<div class="clear"></div>

		<p class="form-row form-row-wide">
			<label for="reg_billing_address_2"><?php _e( 'Address Line 2', 'woocommerce' ); ?></label>
			<input type="text" class="input-text" name="billing_address_2" id="reg_billing_address_2" placeholder="Apartment, suite, unit etc. (optional)" value="<?php if ( ! empty( $_POST['billing_address_2'] ) ) esc_attr_e( $_POST['billing_address_2'] ); ?>" />
		</p>
	  
		<div class="clear"></div>

		<p class="form-row form-row-wide">
			<label for="reg_billing_city"><?php _e( 'Town/City', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_city" id="reg_billing_city" value="<?php if ( ! empty( $_POST['billing_city'] ) ) esc_attr_e( $_POST['billing_city'] ); ?>" />
		</p>
	  
		<p class="form-row form-row-first">
			<label for="reg_billing_state"><?php _e( 'Province', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text js_field-state" name="billing_state" id="reg_billing_state" style="max-width:100%" value="<?php if ( ! empty( $_POST['billing_state'] ) ) esc_attr_e( $_POST['billing_state'] ); ?>" />
		</p>
	  
		<p class="form-row form-row-last">
			<label for="reg_billing_postcode"><?php _e( 'Postal Code', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="input-text" name="billing_postcode" id="reg_billing_postcode" value="<?php if ( ! empty( $_POST['billing_postcode'] ) ) esc_attr_e( $_POST['billing_postcode'] ); ?>" />
		</p>
	  
		<div class="clear"></div>
		
		<p class="form-row form-row-wide">
			<label for="reg_billing_country"><?php _e( 'Country', 'woocommerce' ); ?> <span class="required">*</span></label>
			<select class="country_select js_field-country" name="billing_country" id="reg_billing_country">
				<?php foreach ($countries as $key => $value): ?>
				<option value="<?php echo $key?>"><?php echo $value?></option>
				<?php endforeach; ?>
			</select>
		</p>
	</fieldset>

	<style>
		.form-table .select2-container {
			max-width: 100%;
		}
		.form-table .select2-container .select2-choice {
			padding: 7px 0 6px 8px;
			border-color: #e4e4e4;
			border-radius: 0;
		}
	</style>

	<?php
}

add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

/**
 * Validate the extra register fields.
 *
 * @param  string $username          Current username.
 * @param  string $email             Current email.
 * @param  object $validation_errors WP_Error object.
 *
 * @return void
 */
function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
	if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
		$validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: Your First Name is required!', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
		$validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Your Last Name is required!.', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
		$validation_errors->add( 'billing_phone_error', __( '<strong>Error</strong>: Your Phone is required!.', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
		$validation_errors->add( 'billing_address_1_error', __( '<strong>Error</strong>: Your Address is required!.', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
		$validation_errors->add( 'billing_city_error', __( '<strong>Error</strong>: Your Town/City is required!.', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
		$validation_errors->add( 'billing_postcode_error', __( '<strong>Error</strong>: Your Postal Code is required!.', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_state'] ) && empty( $_POST['billing_state'] ) ) {
		$validation_errors->add( 'billing_state_error', __( '<strong>Error</strong>: Your Province is required!.', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_country'] ) && empty( $_POST['billing_country'] ) ) {
		$validation_errors->add( 'billing_country_error', __( '<strong>Error</strong>: Your Country is required!.', 'woocommerce' ) );
	}

}

add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );

/**
 * Save the extra register fields.
 *
 * @param  int  $customer_id Current customer ID.
 *
 * @return void
 */
function wooc_save_extra_register_fields( $customer_id ) {

  	if ( isset( $_POST['billing_first_name'] ) ) {
		// WordPress default first name field.
		update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );

		// WooCommerce billing first name.
		update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
	}

	if ( isset( $_POST['billing_last_name'] ) ) {
		// WordPress default last name field.
		update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );

		// WooCommerce billing last name.
		update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
	}

	if ( isset( $_POST['billing_phone'] ) ) {
		// WooCommerce billing phone
		update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
	}

	if ( isset( $_POST['billing_address_1'] ) ) {
		// WooCommerce billing address line 1
		update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
	}

	if ( isset( $_POST['billing_address_2'] ) ) {
		// WooCommerce billing address line 2
		update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
	}

	if ( isset( $_POST['billing_city'] ) ) {
		// WooCommerce billing city
		update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
	}

	if ( isset( $_POST['billing_postcode'] ) ) {
		// WooCommerce billing postal/zip code
		update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
	}
	if ( isset( $_POST['billing_country'] ) ) {
		// WooCommerce billing country
		update_user_meta( $customer_id, 'billing_country', sanitize_text_field( $_POST['billing_country'] ) );
	}

	if ( isset( $_POST['email'] ) ) {
		// WooCommerce billing email.
		update_user_meta( $customer_id, 'billing_email', sanitize_text_field( $_POST['email'] ) );
	}
  
}

add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );

/**
 * Enqueue scripts needed for the country and state field
 */
add_action( 'wp_enqueue_scripts', function() {

	/**
	 * My account page only.
	 */
	if ( ! is_page( 'my-account' ) ) {
		return;
	}

	/**
	 * Logged out usersonly.
	 */
	if ( is_user_logged_in() ) {
		return;
	}

	/**
	 * Scripts.
	 */
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	wp_register_script( 'wc-enhanced-select', WC()->plugin_url() . '/assets/js/admin/wc-enhanced-select' . $suffix . '.js', array( 'jquery', 'select2' ), WC_VERSION );
	wp_register_script( 'wc-users', WC()->plugin_url() . '/assets/js/admin/users' . $suffix . '.js', array( 'jquery', 'wc-enhanced-select' ), WC_VERSION );
	wp_enqueue_script( 'wc-users' );
	wp_localize_script( 'wc-users'
		, 'wc_users_params'
		, array(
			'countries'              => json_encode( array_merge( WC()->countries->get_allowed_country_states(), WC()->countries->get_shipping_country_states() ) ),
			'i18n_select_state_text' => esc_attr__( 'Select an option&hellip;', 'woocommerce' ),
		)
	);
} );